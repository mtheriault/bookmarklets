(function(){
console.log('%cAffiche les balises img et leur attribut alt, ainsi que le nombre total d\'images', 'background:#82bdff; color:black;');
console.log('%cAuteur: Mathieu Thériault, pour Ciao', 'background:#82bdff; color:black;');
var img = document.images;
var altArr = [];
for (i=0;i<img.length;i++){
      if (!img[i].hasAttribute("alt")) {
        altArr[i] = img[i] ;
      } else if (img[i].alt == "") {
        altArr[i] = ['!!! ATTRIBUT ALT PRÉSENT MAIS VIDE', img[i]];
      } else {
        altArr[i] = [img[i].alt, img[i]];
        }
      console.log(altArr[i]);
}
console.log("nombre d'images: "+ img.length);

// Script permettant de sortir un array de tout les SVG contenus dans la page, pour ensuite pouvoir les inspecter.
console.log('%cAffiche toutes les balises svg de la page', 'background:#82bdff; color:black;');
console.log('%cAuteur: Mathieu Thériault, pour Ciao', 'background:#82bdff; color:black;');
var SvgArray = document.getElementsByTagName("svg");
for(i=0;i<SvgArray.length;i++){
console.log(SvgArray[i]);
}

// Script me permettant de lister tout les objets ayant l'attribut «background-image»
console.log('%cAffiche les éléments ayant la propriété CSS "background-image", et la valeur de cette propriétée', 'background:#82bdff; color:black;');
console.log('%cAuteur: Mathieu Thériault, pour Ciao', 'background:#82bdff; color:black;');
var objectList = document.querySelectorAll("*");
for (i=0;i<objectList.length;i++){
  var tempProperty = window.getComputedStyle(objectList[i]).getPropertyValue("background-image"); // Pas moyen par hasard d'utiliser un sélecteur d'attribut ? du style [background-image = *] (j'pense pas que c'est ok, mais qqc du genre)
   if (tempProperty !== "none"){                                                                  // Ça aiderait à supprimer ce if là. Et à avoir un array déjà correct.
  console.log(objectList[i],tempProperty);
}
}

/*

Ci-dessous, un programme qui sert à déterminer si la page viole certaines règles d'imbrication HTML5 du W3C.

Le programme peut être de beaucoup optimisé. À titre d'exemple, querySelectorAll() est vraiment moins rapide que getElementsByTagName() ou getElementsByClassName(), etc.
De plus, la fonction ne devrait renvoyer que les éléments allant à l'encontre des spécifications.
Le programme pourrait aussi vérifier certain attributs des éléments, parce que sous certaines conditions relatives aux attributs, certaines imbrications peuvent être autorisées.

*/
console.log('%cAffiche certaines informations sur la page (titre et codet de langue) et vérifie certaines imbrications couramment fautives en HTML5, pour le critère "analyse synthaxique"', 'background:#82bdff; color:black; ');
console.log('%cAuteur: Mathieu Thériault, pour Ciao', 'background:#82bdff; color:black;');
function imbricationV2(tag1,tag2){
  tag2 = tag2 || "html";
  var quantity = document.querySelectorAll(tag2+" "+tag1).length;
  var msg = " <"+tag1+"> dans <"+tag2+"> : "+quantity;
  if(quantity==0 || null){
    return console.log("  Aucune balise <"+tag1+"> dans une balise <"+tag2+">");
  }else{
    return console.error(msg);
  }
}
var cL = console.warn;

cL("Titre de la page");
console.log(document.getElementsByTagName("title")[0].innerText);

cL("Langue de la page");
console.log(document.getElementsByTagName("html")[0].getAttribute("lang"));

cL("Seul le «phrasing content» est permis dans un <span>");
imbricationV2("div","span");
imbricationV2("p","span");
imbricationV2("ul","span");
imbricationV2("ol","span");
imbricationV2("li","span");
imbricationV2("article","span");

cL("Aucun élément interactifs descendant d'un <a> (voir conditions spécifiques à certains types d'objets)")
imbricationV2("a","a");
imbricationV2("button","a");
imbricationV2("embed","a");
imbricationV2("iframe","a");
imbricationV2("keygen","a");
imbricationV2("label","a");
imbricationV2("select","a");
imbricationV2("textarea","a");
cL("(Pour un <input> dans un <a>, c'est correct si le <input> possède l'attribut type ayant la valeur hidden)");
imbricationV2("input","a");
cL("(Pour un <audio> ou <video> dans un <a>, c'est correct si le <audio> ou <video> ne possède pas un attribut controls)");
imbricationV2("audio","a");
imbricationV2("video","a");
cL("(Pour un <img> ou un <object> dans un <a>, c'est correct si le <img> ou le <object> ne possède pas d'attribut usemap)");
imbricationV2("img","a");
imbricationV2("object","a");

cL("Autres checkups");
imbricationV2("img");
imbricationV2("svg");

// Liste les contenus des Hx et donne un lien vers les Hx dans le code
var array1 = document.getElementsByTagName("h1"),
array2 = document.getElementsByTagName("h2"),
array3 = document.getElementsByTagName("h3"),
array4 = document.getElementsByTagName("h4"),
array5 = document.getElementsByTagName("h5"),
array6 = document.getElementsByTagName("h6"),
arrayX = [];

for (i=0;i<array1.length;i++){
arrayX[i] = array1[i].textContent;
console.log("H1 contenu: "+arrayX[i], array1[i]);
}
for (i=0;i<array2.length;i++){
arrayX[i] = array2[i].textContent;
console.log("H2 contenu: "+arrayX[i], array2[i]);
}
for (i=0;i<array3.length;i++){
arrayX[i] = array3[i].textContent;
console.log("H3 contenu: "+arrayX[i], array3[i]);
}
for (i=0;i<array4.length;i++){
arrayX[i] = array4[i].textContent;
console.log("H4 contenu: "+arrayX[i], array4[i]);
}
for (i=0;i<array5.length;i++){
arrayX[i] = array5[i].textContent;
console.log("H5 contenu: "+arrayX[i], array5[i]);
}
for (i=0;i<array6.length;i++){
arrayX[i] = array6[i].textContent;
console.log("H6 contenu: "+arrayX[i], array6[i]);
}

// Plus léger
var h1=document.getElementsByTagName("h1"),h2=document.getElementsByTagName("h2"),h3=document.getElementsByTagName("h3"),h4=document.getElementsByTagName("h4"),h5=document.getElementsByTagName("h5"),h6=document.getElementsByTagName("h6"),aX=[];for(i=0;i<h1.length;i++)aX[i]=h1[i].textContent,console.log("H1 contenu: "+aX[i],h1[i]);for(i=0;i<h2.length;i++)aX[i]=h2[i].textContent,console.log("H2 contenu: "+aX[i],h2[i]);for(i=0;i<h3.length;i++)aX[i]=h3[i].textContent,console.log("H3 contenu: "+aX[i],h3[i]);for(i=0;i<h4.length;i++)aX[i]=h4[i].textContent,console.log("H4 contenu: "+aX[i],h4[i]);for(i=0;i<h5.length;i++)aX[i]=h5[i].textContent,console.log("H5 contenu: "+aX[i],h5[i]);for(i=0;i<h6.length;i++)aX[i]=h6[i].textContent,console.log("H6 contenu: "+aX[i],h6[i]);

//Pour sélectionner tout les liens et afficher dans la console les a avec un role autre que lien.
var liens = document.querySelectorAll("a");
var rolesArr = [];

for (i=0;i<liens.length;i++){
    if (!liens[i].hasAttribute("role")) {
     rolesArr[i] = ['lien sans role:', liens[i].innerText, liens[i]] ;
  } else {
     rolesArr[i] = [liens[i].getAttribute("role"),liens[i].innerText, liens[i]];}
    console.log(rolesArr[i]);}
console.log("nombre de liens: "+ liens.length);


// Pour trouver les meta name=viewport, changeant la possibilité de zoom sur mobile (à améliorer pour ne viser que les viewport)
var arrayA = document.getElementsByTagName("meta");
for(i=0;i<arrayA.length;i++){
    console.log(arrayA[i].name, arrayA[i]);
}})();
